#ifndef CSFGUI_ENGINE_H
#define CSFGUI_ENGINE_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

CSFGUI_API void sfgEngine_Destroy(sfgEngine* Engine);
CSFGUI_API sfBool sfgEngine_IsEqual(sfgEngine* A, sfgEngine* B);

#endif
