#ifndef CSFGUI_WINDOW_H
#define CSFGUI_WINDOW_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>
#include <SFML/Graphics/Rect.h>

typedef enum sfgWindowStyle : sfUint8
{
    NoStyle = 0,
    Titlebar = 1 << 0,
    Background = 1 << 1,
    Resize = 1 << 2,
    Shadow = 1 << 3,
    TopLevel = Titlebar | Background | Resize
};

CSFGUI_API sfgWindow* sfgWindow_Create(sfgWindowStyle Style);
CSFGUI_API void sfgWindow_Destroy(sfgWindow* Window);
CSFGUI_API sfFloatRect sfgWindow_GetClientRect(sfgWindow* Window);
CSFGUI_API sfgWindowStyle sfgWindow_GetStyle(sfgWindow* Window);
CSFGUI_API const char* sfgWindow_GetTitle(sfgWindow* Window);
CSFGUI_API sfBool sfgWindow_HasStyle(sfgWindow* Window, sfgWindowStyle Style);
CSFGUI_API void sfgWindow_SetStyle(sfgWindow* Window, sfgWindowStyle Style);
CSFGUI_API void sfgWindow_SetTitle(sfgWindow* Window, const char* Title);
CSFGUI_API sfgBin* sfgWindow_GetBaseBin(sfgWindow* Window);
CSFGUI_API sfBool sfgWindow_IsEqual(sfgWindow* A, sfgWindow* B);

#endif
