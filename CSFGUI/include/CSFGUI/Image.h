#ifndef CSFGUI_IMAGE_H
#define CSFGUI_IMAGE_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>
#include <SFML/Graphics/Image.h>

CSFGUI_API sfgImage* sfgImage_Create(sfImage* Image);
CSFGUI_API void sfgImage_Destroy(sfgImage* Image);
CSFGUI_API void sfgImage_GetImage(sfgImage* Image, sfImage* SFImage);
CSFGUI_API void sfgImage_SetImage(sfgImage* Image, sfImage* SFImage);
CSFGUI_API sfgWidget* sfgImage_GetBaseWidget(sfgImage* Image);
CSFGUI_API sfBool sfgImage_IsEqual(sfgImage* A, sfgImage* B);

#endif
