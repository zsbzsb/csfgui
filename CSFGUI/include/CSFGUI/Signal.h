#ifndef CSFGUI_SIGNAL_H
#define CSFGUI_SIGNAL_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

typedef void(*sfgSignalCallback)();

CSFGUI_API void sfgSignal_Destroy(sfgSignal* Signal);
CSFGUI_API sfUint32 sfgSignal_ConnectCallback(sfgSignal* Signal, sfgSignalCallback Callback);
CSFGUI_API void sfgSignal_DisconnectCallback(sfgSignal* Signal, sfUint32 CallbackID);

#endif
