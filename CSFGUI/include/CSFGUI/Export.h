#ifndef CSFGUI_EXPORT_H
#define CSFGUI_EXPORT_H

#include <CSFGUI/Config.h>

#if defined(CSFGUI_EXPORTS)
	#define CSFGUI_API CSFGUI_API_EXPORT
#else
    #define CSFGUI_API CSFGUI_API_IMPORT
#endif

#endif
