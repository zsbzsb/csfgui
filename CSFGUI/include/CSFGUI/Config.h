#ifndef CSFGUI_CONFIG_H
#define CSFGUI_CONFIG_H

#if defined(_WIN32) || defined(__WIN32__)
    #define CSFGUI_SYSTEM_WINDOWS
#elif defined(linux) || defined(__linux)
    #define CSFGUI_SYSTEM_LINUX
#elif defined(__APPLE__) || defined(MACOSX) || defined(macintosh) || defined(Macintosh)
    #define CSFGUI_SYSTEM_MACOS
#elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
    #define CSFGUI_SYSTEM_FREEBSD
#else
    #error This operating system is not supported
#endif

#if defined(CSFGUI_SYSTEM_WINDOWS)
    #define CSFGUI_API_EXPORT extern "C" __declspec(dllexport)
    #define CSFGUI_API_IMPORT extern __declspec(dllimport)
    #ifdef _MSC_VER
        #pragma warning(disable : 4251)
    #endif
#else
    #if __GNUC__ >= 4
        #define CSFGUI_API_EXPORT extern "C" __attribute__ ((__visibility__ ("default")))
        #define CSFGUI_API_IMPORT extern __attribute__ ((__visibility__ ("default")))
    #else
        #define CSFGUI_API_EXPORT extern "C"
        #define CSFGUI_API_IMPORT extern
    #endif
#endif

#endif
