#ifndef CSFGUI_OBJECT_H
#define CSFGUI_OBJECT_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>
#include <cstddef>

CSFGUI_API void sfgObject_Destroy(sfgObject* Object);
CSFGUI_API sfgSignal* sfgObject_GetSignal(sfgObject* Object, std::size_t SignalID);

#endif
