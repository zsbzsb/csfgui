#ifndef CSFGUI_RADIOBUTTONGROUP_H
#define CSFGUI_RADIOBUTTONGROUP_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

CSFGUI_API sfgRadioButtonGroup* sfgRadioButtonGroup_Create();
CSFGUI_API void sfgRadioButtonGroup_Destroy(sfgRadioButtonGroup* RadioButtonGroup);
CSFGUI_API sfBool sfgRadioButtonGroup_IsEqual(sfgRadioButtonGroup* A, sfgRadioButtonGroup* B);
CSFGUI_API sfgRadioButton* sfgRadioButtonGroup_GetMemberByIndex(sfgRadioButtonGroup* RadioButtonGroup, sfInt32 Index);
CSFGUI_API sfInt32 sfgRadioButtonGroup_GetMemberCount(sfgRadioButtonGroup* RadioButtonGroup);

#endif
