#ifndef CSFGUI_DESKTOP_H
#define CSFGUI_DESKTOP_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>
#include <SFML/Window/Event.h>

CSFGUI_API sfgDesktop* sfgDesktop_Create();
CSFGUI_API void sfgDesktop_Destroy(sfgDesktop* Desktop);
CSFGUI_API void sfgDesktop_Add(sfgDesktop* Desktop, sfgWidget* Widget);
CSFGUI_API void sfgDesktop_BringToFront(sfgDesktop* Desktop, sfgWidget* Widget);
CSFGUI_API sfgEngine* sfgDesktop_GetEngine(sfgDesktop* Desktop);
CSFGUI_API void sfgDesktop_HandleEvent(sfgDesktop* Desktop, sfEvent* Event);
CSFGUI_API sfBool sfgDesktop_LoadThemeFromFile(sfgDesktop* Desktop, const char* Filename);
CSFGUI_API void sfgDesktop_Refresh(sfgDesktop* Desktop);
CSFGUI_API void sfgDesktop_Remove(sfgDesktop* Desktop, sfgWidget* Widget);
CSFGUI_API void sfgDesktop_RemoveAll(sfgDesktop* Desktop);
CSFGUI_API void sfgDesktop_Update(sfgDesktop* Desktop, float Seconds);
// TODO finish get/set properties - can throw exceptions

#endif
