#ifndef CSFGUI_WIDGET_H
#define CSFGUI_WIDGET_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>
#include <SFML/System/Vector2.h>
#include <SFML/Graphics/Rect.h>
#include <SFML/Window/Event.h>
#include <SFML/ConvertEvent.h>

typedef enum sfgWidgetState : sfUint8
{
    Normal = 0,
    Active = 1,
    Prelight = 2,
    Selected = 3,
    Insensitive = 4
};

CSFGUI_API sfVector2f sfgWidget_GetAbsolutePosition(sfgWidget* Widget);
CSFGUI_API sfFloatRect sfgWidget_GetAllocation(sfgWidget* Widget);
CSFGUI_API const char* sfgWidget_GetName(sfgWidget* Widget);
CSFGUI_API const char* sfgWidget_GetClass(sfgWidget* Widget);
CSFGUI_API const char* sfgWidget_GetId(sfgWidget* Widget);
CSFGUI_API sfInt32 sfgWidget_GetHierarchyLevel(sfgWidget* Widget);
CSFGUI_API sfgContainer* sfgWidget_GetParent(sfgWidget* Widget);
CSFGUI_API sfVector2f sfgWidget_GetRequisition(sfgWidget* Widget);
CSFGUI_API sfgWidgetState sfgWidget_GetState(sfgWidget* Widget);
CSFGUI_API sfgRendererViewport* sfgWidget_GetViewport(sfgWidget* Widget);
CSFGUI_API sfInt32 sfgWidget_GetZOrder(sfgWidget* Widget);
CSFGUI_API void sfgWidget_GrabFocus(sfgWidget* Widget);
CSFGUI_API void sfgWidget_HandleAbsolutePositionChange(sfgWidget* Widget);
CSFGUI_API void sfgWidget_HandleEvent(sfgWidget* Widget, sfEvent* Event);
CSFGUI_API void sfgWidget_HandleGlobalVisibilityChange(sfgWidget* Widget);
CSFGUI_API sfBool sfgWidget_HasFocus(sfgWidget* Widget);
CSFGUI_API void sfgWidget_Invalidate(sfgWidget* Widget);
CSFGUI_API sfBool sfgWidget_IsActiveWidget(sfgWidget* Widget);
CSFGUI_API sfBool sfgWidget_IsGloballyVisible(sfgWidget* Widget);
CSFGUI_API sfBool sfgWidget_IsLocallyVisible(sfgWidget* Widget);
CSFGUI_API void sfgWidget_Refresh(sfgWidget* Widget);
CSFGUI_API void sfgWidget_RequestResize(sfgWidget* Widget);
CSFGUI_API void sfgWidget_SetActiveWidget(sfgWidget* Widget);
CSFGUI_API void sfgWidget_SetAllocation(sfgWidget* Widget, sfFloatRect Rect);
CSFGUI_API void sfgWidget_SetClass(sfgWidget* Widget, const char* Class);
CSFGUI_API void sfgWidget_SetHierarchyLevel(sfgWidget* Widget, sfInt32 Level);
CSFGUI_API void sfgWidget_SetId(sfgWidget* Widget, const char* Id);
CSFGUI_API void sfgWidget_SetParent(sfgWidget* Widget, sfgWidget* Parent);
CSFGUI_API void sfgWidget_SetPosition(sfgWidget* Widget, sfVector2f Position);
CSFGUI_API void sfgWidget_SetRequisition(sfgWidget* Widget, sfVector2f Requisition);
CSFGUI_API void sfgWidget_SetState(sfgWidget* Widget, sfgWidgetState State);
CSFGUI_API void sfgWidget_SetViewport(sfgWidget* Widget, sfgRendererViewport* Viewport);
CSFGUI_API void sfgWidget_SetZOrder(sfgWidget* Widget, sfInt32 ZOrder);
CSFGUI_API void sfgWidget_Show(sfgWidget* Widget, sfBool Show);
CSFGUI_API void sfgWidget_Update(sfgWidget* Widget, float Seconds);
CSFGUI_API void sfgWidget_UpdateDrawablePosition(sfgWidget* Widget);
CSFGUI_API void sfgWidget_Destroy(sfgWidget* Widget);
CSFGUI_API sfgObject* sfgWidget_GetBaseObject(sfgWidget* Widget);
CSFGUI_API sfBool sfgWidget_IsEqual(sfgWidget* A, sfgWidget* B);

#endif
