#include <CSFGUI/Button.h>
#include <CSFGUI/ButtonStruct.h>
#include <CSFGUI/BinStruct.h>
#include <CSFGUI/ImageStruct.h>
#include <SFML/Graphics/ImageStruct.h>

sfgButton* sfgButton_Create(const char* Label)
{
    sfgButton* button = new sfgButton;
    button->Handle = sfg::Button::Create(Label);
    return button;
}

void sfgButton_Destroy(sfgButton* Button)
{
    delete Button;
}

void sfgButton_ClearImage(sfgButton* Button)
{
    Button->Handle->ClearImage();
}

sfgImage* sfgButton_GetImage(sfgButton* Button)
{
    auto imageptr = Button->Handle->GetImage();
    if (imageptr)
    {
        sfgImage* image = new sfgImage;
        image->Handle = imageptr;
        return image;
    }
    else return nullptr;
}

const char* sfgButton_GetLabel(sfgButton* Button)
{
    Button->String = Button->Handle->GetLabel().toAnsiString();
    return Button->String.c_str();
}

void sfgButton_SetImage(sfgButton* Button, sfgImage* Image)
{
    Button->Handle->SetImage(Image->Handle);
}

void sfgButton_SetLabel(sfgButton* Button, const char* Label)
{
    Button->Handle->SetLabel(Label);
}

sfgBin* sfgButton_GetBaseBin(sfgButton* Button)
{
    sfgBin* bin = new sfgBin;
    bin->Handle = Button->Handle;
    return bin;
}

sfBool sfgButton_IsEqual(sfgButton* A, sfgButton* B)
{
    return A->Handle == B->Handle;
}
