#include <CSFGUI/ToggleButton.h>
#include <CSFGUI/ToggleButtonStruct.h>
#include <CSFGUI/ButtonStruct.h>

sfgToggleButton* sfgToggleButton_Create(const char* Label)
{
    sfgToggleButton* togglebutton = new sfgToggleButton;
    togglebutton->Handle = sfg::ToggleButton::Create(Label);
    return togglebutton;
}

void sfgToggleButton_Destroy(sfgToggleButton* ToggleButton)
{
    delete ToggleButton;
}

sfgButton* sfgToggleButton_GetBaseButton(sfgToggleButton* ToggleButton)
{
    sfgButton* button = new sfgButton;
    button->Handle = ToggleButton->Handle;
    return button;
}

sfBool sfgToggleButton_IsEqual(sfgToggleButton* A, sfgToggleButton* B)
{
    return A->Handle == B->Handle;
}

sfBool sfgToggleButton_IsActive(sfgToggleButton* ToggleButton)
{
    return ToggleButton->Handle->IsActive();
}

void sfgToggleButton_SetActive(sfgToggleButton* ToggleButton, sfBool Active)
{
    ToggleButton->Handle->SetActive(static_cast<bool>(Active));
}
