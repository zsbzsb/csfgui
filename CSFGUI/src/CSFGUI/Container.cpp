#include <CSFGUI/Container.h>
#include <CSFGUI/ContainerStruct.h>
#include <CSFGUI/WidgetStruct.h>

void sfgContainer_Destroy(sfgContainer* Container)
{
    delete Container;
}

void sfgContainer_Add(sfgContainer* Container, sfgWidget* Widget)
{
    Container->Handle->Add(Widget->Handle);
}

sfgWidget* sfgContainer_GetChildByIndex(sfgContainer* Container, sfInt32 Index)
{
    sfgWidget* widget = new sfgWidget;
    widget->Handle = Container->Handle->GetChildren()[Index];
    return widget;
}

sfInt32 sfgContainer_GetChildrenCount(sfgContainer* Container)
{
    return Container->Handle->GetChildren().size();
}

void sfgContainer_HandleChildInvalidate(sfgContainer* Container, sfgWidget* Widget)
{
    Container->Handle->HandleChildInvalidate(Widget->Handle);
}

sfBool sfgContainer_IsChild(sfgContainer* Container, sfgWidget* Widget)
{
    return Container->Handle->IsChild(Widget->Handle);
}

void sfgContainer_Remove(sfgContainer* Container, sfgWidget* Widget)
{
    Container->Handle->Remove(Widget->Handle);
}

void sfgContainer_RemoveAll(sfgContainer* Container)
{
    Container->Handle->RemoveAll();
}

sfgWidget* sfgContainer_GetBaseWidget(sfgContainer* Container)
{
    sfgWidget* widget = new sfgWidget;
    widget->Handle = Container->Handle;
    return widget;
}

sfBool sfgContainer_IsEqual(sfgContainer* A, sfgContainer* B)
{
    return A->Handle == B->Handle;
}
