#include <CSFGUI/Label.h>
#include <CSFGUI/LabelStruct.h>
#include <CSFGUI/WidgetStruct.h>

sfgLabel* sfgLabel_Create(const char* Text)
{
    sfgLabel* label = new sfgLabel;
    label->Handle = sfg::Label::Create(Text);
    return label;
}

void sfgLabel_Destroy(sfgLabel* Label)
{
    delete Label;
}

sfBool sfgLabel_GetLineWrap(sfgLabel* Label)
{
    return Label->Handle->GetLineWrap();
}

const char* sfgLabel_GetText(sfgLabel* Label)
{
    Label->String = Label->Handle->GetText().toAnsiString();
    return Label->String.c_str();
}

const char* sfgLabel_GetWrappedText(sfgLabel* Label)
{
    Label->String = Label->Handle->GetWrappedText().toAnsiString();
    return Label->String.c_str();
}

void sfgLabel_SetLineWrap(sfgLabel* Label, sfBool Wrap)
{
    Label->Handle->SetLineWrap(static_cast<bool>(Wrap));
}

void sfgLabel_SetText(sfgLabel* Label, const char* Text)
{
    Label->Handle->SetText(Text);
}

sfgWidget* sfgLabel_GetBaseWidget(sfgLabel* Label)
{
    sfgWidget* widget = new sfgWidget;
    widget->Handle = Label->Handle;
    return widget;
}

sfBool sfgLabel_IsEqual(sfgLabel* A, sfgLabel* B)
{
    return A->Handle == B->Handle;
}
