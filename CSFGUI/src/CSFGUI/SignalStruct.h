#ifndef CSFGUI_SIGNALSTRUCT_H
#define CSFGUI_SIGNALSTRUCT_H

#include <SFGUI/Signal.hpp>

struct sfgSignal
{
    sfg::Signal* Handle;
};

#endif
