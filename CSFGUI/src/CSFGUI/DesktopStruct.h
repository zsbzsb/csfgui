#ifndef CSFGUI_DESKTOPSTRUCT_H
#define CSFGUI_DESKTOPSTRUCT_H

#include <SFGUI/Desktop.hpp>

struct sfgDesktop
{
    sfg::Desktop Handle;
};

#endif
