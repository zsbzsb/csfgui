#ifndef CSFGUI_RAIDOBUTTONSTRUCT_H
#define CSFGUI_RADIOBUTTONSTRUCT_H

#include <SFGUI/RadioButton.hpp>

struct sfgRadioButton
{
    sfg::RadioButton::Ptr Handle;
};

#endif
