#ifndef CSFGUI_CONTAINERSTRUCT_H
#define CSFGUI_CONTAINERSTRUCT_H

#include <SFGUI/Container.hpp>

struct sfgContainer
{
    sfg::Container::Ptr Handle;
};

#endif
