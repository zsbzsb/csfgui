#include <CSFGUI/Window.h>
#include <CSFGUI/WindowStruct.h>
#include <CSFGUI/BinStruct.h>

sfgWindow* sfgWindow_Create(sfgWindowStyle Style)
{
    sfgWindow* window = new sfgWindow;
    window->Handle = sfg::Window::Create(static_cast<sfg::Window::Style>(Style));
    return window;
}

void sfgWindow_Destroy(sfgWindow* Window)
{
    delete Window;
}

sfFloatRect sfgWindow_GetClientRect(sfgWindow* Window)
{
    sf::FloatRect rect = Window->Handle->GetClientRect();
    return{ rect.left, rect.top, rect.width, rect.height };
}

sfgWindowStyle sfgWindow_GetStyle(sfgWindow* Window)
{
    return static_cast<sfgWindowStyle>(Window->Handle->GetStyle());
}

const char* sfgWindow_GetTitle(sfgWindow* Window)
{
    Window->String = Window->Handle->GetTitle().toAnsiString();
    return Window->String.c_str();
}

sfBool sfgWindow_HasStyle(sfgWindow* Window, sfgWindowStyle Style)
{
    return Window->Handle->HasStyle(static_cast<sfg::Window::Style>(Style));
}

void sfgWindow_SetStyle(sfgWindow* Window, sfgWindowStyle Style)
{
    Window->Handle->SetStyle(static_cast<sfg::Window::Style>(Style));
}

void sfgWindow_SetTitle(sfgWindow* Window, const char* Title)
{
    Window->Handle->SetTitle(Title);
}

sfgBin* sfgWindow_GetBaseBin(sfgWindow* Window)
{
    sfgBin* bin = new sfgBin;
    bin->Handle = Window->Handle;
    return bin;
}

sfBool sfgWindow_IsEqual(sfgWindow* A, sfgWindow* B)
{
    return A->Handle == B->Handle;
}
