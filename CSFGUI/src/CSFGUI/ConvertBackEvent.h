////////////////////////////////////////////////////////////
//
// SFML - Simple and Fast Multimedia Library
// Copyright (C) 2007-2009 Laurent Gomila (laurent.gom@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef CSFGUI_CONVERTBACKEVENT_H
#define CSFGUI_CONVERTBACKEVENT_H

#include <SFML/Window/Event.hpp>
#include <SFML/Window/Event.h>

inline void ConvertBackEvent(sfEvent& CEvent, sf::Event* SEvent)
{
    // Convert its type
    SEvent->type = static_cast<sf::Event::EventType>(CEvent.type);

    // Fill its fields
    switch (SEvent->type)
    {
    case sf::Event::Resized:
        SEvent->size.width = CEvent.size.width;
        SEvent->size.height = CEvent.size.height;
        break;

    case sf::Event::TextEntered:
        SEvent->text.unicode = CEvent.text.unicode;
        break;

    case sf::Event::KeyReleased:
    case sf::Event::KeyPressed:
        SEvent->key.code = static_cast<sf::Keyboard::Key>(CEvent.key.code);
        SEvent->key.alt = CEvent.key.alt ? sfTrue : sfFalse;
        SEvent->key.control = CEvent.key.control ? sfTrue : sfFalse;
        SEvent->key.shift = CEvent.key.shift ? sfTrue : sfFalse;
        SEvent->key.system = CEvent.key.system ? sfTrue : sfFalse;
        break;

    case sf::Event::MouseWheelMoved:
        SEvent->mouseWheel.delta = CEvent.mouseWheel.delta;
        SEvent->mouseWheel.x = CEvent.mouseWheel.x;
        SEvent->mouseWheel.y = CEvent.mouseWheel.y;
        break;

    case sf::Event::MouseButtonPressed:
    case sf::Event::MouseButtonReleased:
        SEvent->mouseButton.button = static_cast<sf::Mouse::Button>(CEvent.mouseButton.button);
        SEvent->mouseButton.x = CEvent.mouseButton.x;
        SEvent->mouseButton.y = CEvent.mouseButton.y;
        break;

    case sf::Event::MouseMoved:
        SEvent->mouseMove.x = CEvent.mouseMove.x;
        SEvent->mouseMove.y = CEvent.mouseMove.y;
        break;

    case sf::Event::JoystickButtonPressed:
    case sf::Event::JoystickButtonReleased:
        SEvent->joystickButton.joystickId = CEvent.joystickButton.joystickId;
        SEvent->joystickButton.button = CEvent.joystickButton.button;
        break;

    case sf::Event::JoystickMoved:
        SEvent->joystickMove.joystickId = CEvent.joystickMove.joystickId;
        SEvent->joystickMove.axis = static_cast<sf::Joystick::Axis>(CEvent.joystickMove.axis);
        SEvent->joystickMove.position = CEvent.joystickMove.position;
        break;

    case sf::Event::JoystickConnected:
    case sf::Event::JoystickDisconnected:
        SEvent->joystickConnect.joystickId = CEvent.joystickConnect.joystickId;
        break;

    default:
        break;
    }
}

#endif
