#include <CSFGUI/SFGUI.h>
#include <CSFGUI/SFGUIStruct.h>
#include <CSFGUI/RendererStruct.h>
#include <SFML/Window/WindowStruct.h>
#include <SFML/Graphics/RenderWindowStruct.h>
#include <SFML/Graphics/RenderTextureStruct.h>

sfgSFGUI* sfgSFGUI_Create()
{
    return new sfgSFGUI;
}

void sfgSFGUI_Destroy(sfgSFGUI* SFGUI)
{
    delete SFGUI;
}

void sfgSFGUI_DisplayWindow(sfgSFGUI* SFGUI, sfWindow* Window)
{
    SFGUI->Handle.Display(Window->This);
}

void sfgSFGUI_DisplayRenderWindow(sfgSFGUI* SFGUI, sfRenderWindow* RenderWindow)
{
    SFGUI->Handle.Display(RenderWindow->This);
}

void sfgSFGUI_DisplayRenderTexture(sfgSFGUI* SFGUI, sfRenderTexture* RenderTexture)
{
    SFGUI->Handle.Display(RenderTexture->This);
}

sfgRenderer* sfgSFGUI_GetRenderer(sfgSFGUI* SFGUI)
{
    sfgRenderer* renderer = new sfgRenderer;
    renderer->Handle = &SFGUI->Handle.GetRenderer();
    return renderer;
}
