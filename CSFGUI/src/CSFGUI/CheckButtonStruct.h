#ifndef CSFGUI_CHECKBUTTONSTRUCT_H
#define CSFGUI_CHECKBUTTONSTRUCT_H

#include <SFGUI/CheckButton.hpp>

struct sfgCheckButton
{
    sfg::CheckButton::Ptr Handle;
};

#endif
