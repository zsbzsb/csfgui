#ifndef CSFGUI_OBJECTSTRUCT_H
#define CSFGUI_OBJECTSTRUCT_H

#include <SFGUI/Object.hpp>

struct sfgObject
{
    sfg::Object::Ptr Handle;
};

#endif
