#include <CSFGUI/Object.h>
#include <CSFGUI/ObjectStruct.h>
#include <CSFGUI/SignalStruct.h>

void sfgObject_Destroy(sfgObject* Object)
{
    delete Object;
}

sfgSignal* sfgObject_GetSignal(sfgObject* Object, std::size_t SignalID)
{
    sfgSignal* signal = new sfgSignal;
    signal->Handle = &Object->Handle->GetSignal(SignalID);
    return signal;
}
