#include <CSFGUI/Engine.h>
#include <CSFGUI/EngineStruct.h>

void sfgEngine_Destroy(sfgEngine* Engine)
{
    delete Engine;
}

sfBool sfgEngine_IsEqual(sfgEngine* A, sfgEngine* B)
{
    return &A->Handle == &B->Handle;
}
