#ifndef CSFGUI_WIDGETSTRUCT_H
#define CSFGUI_WIDGETSTRUCT_H

#include <SFGUI/Widget.hpp>

struct sfgWidget
{
    sfg::Widget::Ptr Handle;
    std::string String;
};

#endif
