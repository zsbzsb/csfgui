#ifndef CSFGUI_ENGINESTRUCT_H
#define CSFGUI_ENGINESTRUCT_H

#include <SFGUI/Engine.hpp>

struct sfgEngine
{
    sfg::Engine* Handle;
};

#endif
