#ifndef CSFGUI_BUTTONSTRUCT_H
#define CSFGUI_BUTTONSTRUCT_H

#include <SFGUI/Button.hpp>

struct sfgButton
{
    sfg::Button::Ptr Handle;
    std::string String;
};

#endif
