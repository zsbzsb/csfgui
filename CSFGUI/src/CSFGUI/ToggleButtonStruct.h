#ifndef CSFGUI_TOGGLEBUTTONSTRUCT_H
#define CSFGUI_TOGGLEBUTTONSTRUCT_H

#include <SFGUI/ToggleButton.hpp>

struct sfgToggleButton
{
    sfg::ToggleButton::Ptr Handle;
};

#endif
