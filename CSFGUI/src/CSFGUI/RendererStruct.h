#ifndef CSFGUI_RENDERERSTRUCT_H
#define CSFGUI_RENDERERSTRUCT_H

#include <SFGUI/Renderer.hpp>

struct sfgRenderer
{
    sfg::Renderer* Handle;
};

#endif
