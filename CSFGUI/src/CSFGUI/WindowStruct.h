#ifndef CSFGUI_WINDOWSTRUCT_H
#define CSFGUI_WINDOWSTRUCT_H

#include <SFGUI/Window.hpp>

struct sfgWindow
{
    sfg::Window::Ptr Handle;
    std::string String;
};

#endif
