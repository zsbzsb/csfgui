#include <CSFGUI/CheckButton.h>
#include <CSFGUI/CheckButtonStruct.h>
#include <CSFGUI/ToggleButtonStruct.h>

sfgCheckButton* sfgCheckButton_Create(const char* Label)
{
    sfgCheckButton* checkbutton = new sfgCheckButton;
    checkbutton->Handle = sfg::CheckButton::Create(Label);
    return checkbutton;
}

void sfgCheckButton_Destroy(sfgCheckButton* CheckButton)
{
    delete CheckButton;
}

sfgToggleButton* sfgCheckButton_GetBaseToggleButton(sfgCheckButton* CheckButton)
{
    sfgToggleButton* togglebutton = new sfgToggleButton;
    togglebutton->Handle = CheckButton->Handle;
    return togglebutton;
}

sfBool sfgCheckButton_IsEqual(sfgCheckButton* A, sfgCheckButton* B)
{
    return A->Handle == B->Handle;
}
