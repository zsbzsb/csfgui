#include <CSFGUI/SignalID.h>
#include <CSFGUI/ObjectStruct.h>
#include <SFGUI/Signal.hpp>
#include <SFGUI/Widget.hpp>
#include <SFGUI/ToggleButton.hpp>

std::size_t sfgSignalID_GetID(sfgObject* Object, sfgSignalIDType SignalIDType)
{
    sfg::Signal::SignalID* id = nullptr;
    switch (SignalIDType)
    {
        case sfgSignalIDType::Widget_OnStateChange:
            id = &sfg::Widget::OnStateChange; break;
        case sfgSignalIDType::Widget_OnGainFocus:
            id = &sfg::Widget::OnGainFocus; break;
        case sfgSignalIDType::Widget_OnLostFocus:
            id = &sfg::Widget::OnLostFocus; break;
        case sfgSignalIDType::Widget_OnExpose:
            id = &sfg::Widget::OnExpose; break;
        case sfgSignalIDType::Widget_OnSizeAllocate:
            id = &sfg::Widget::OnSizeAllocate; break;
        case sfgSignalIDType::Widget_OnSizeRequest:
            id = &sfg::Widget::OnSizeRequest; break;
        case sfgSignalIDType::Widget_OnMouseEnter:
            id = &sfg::Widget::OnMouseEnter; break;
        case sfgSignalIDType::Widget_OnMouseLeave:
            id = &sfg::Widget::OnMouseLeave; break;
        case sfgSignalIDType::Widget_OnMouseMove:
            id = &sfg::Widget::OnMouseMove; break;
        case sfgSignalIDType::Widget_OnMouseLeftPress:
            id = &sfg::Widget::OnMouseLeftPress; break;
        case sfgSignalIDType::Widget_OnMouseRightPress:
            id = &sfg::Widget::OnMouseRightPress; break;
        case sfgSignalIDType::Widget_OnMouseLeftRelease:
            id = &sfg::Widget::OnMouseLeftRelease; break;
        case sfgSignalIDType::Widget_OnMouseRightRelease:
            id = &sfg::Widget::OnMouseRightRelease; break;
        case sfgSignalIDType::Widget_OnLeftClick:
            id = &sfg::Widget::OnLeftClick; break;
        case sfgSignalIDType::Widget_OnRightClick:
            id = &sfg::Widget::OnRightClick; break;
        case sfgSignalIDType::Widget_OnKeyPress:
            id = &sfg::Widget::OnKeyPress; break;
        case sfgSignalIDType::Widget_OnKeyRelease:
            id = &sfg::Widget::OnKeyRelease; break;
        case sfgSignalIDType::Widget_OnText:
            id = &sfg::Widget::OnText; break;

        case sfgSignalIDType::ToggleButton_OnToggle:
            id = &sfg::ToggleButton::OnToggle; break;

        default:
            return 0;
    }
    Object->Handle->GetSignal(*id);
    return *id;
}
