#include <CSFGUI/RendererViewport.h>
#include <CSFGUI/RendererViewportStruct.h>

void sfgRendererViewport_Destroy(sfgRendererViewport* RendererViewport)
{
    delete RendererViewport;
}

sfBool sfgRendererViewport_IsEqual(sfgRendererViewport* A, sfgRendererViewport* B)
{
    return A->Handle == B->Handle;
}
