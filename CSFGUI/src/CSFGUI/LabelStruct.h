#ifndef CSFGUI_LABELSTRUCT_H
#define CSFGUI_LABELSTRUCT_H

#include <SFGUI/Label.hpp>

struct sfgLabel
{
    sfg::Label::Ptr Handle;
    std::string String;
};

#endif
