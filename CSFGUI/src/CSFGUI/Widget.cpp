#include <CSFGUI/Widget.h>
#include <CSFGUI/WidgetStruct.h>
#include <CSFGUI/ContainerStruct.h>
#include <CSFGUI/RendererViewportStruct.h>
#include <CSFGUI/ObjectStruct.h>
#include <CSFGUI/ConvertBackEvent.h>

sfVector2f sfgWidget_GetAbsolutePosition(sfgWidget* Widget)
{
    sf::Vector2f pos = Widget->Handle->GetAbsolutePosition();
    return { pos.x, pos.y };
}

sfFloatRect sfgWidget_GetAllocation(sfgWidget* Widget)
{
    sf::FloatRect rect = Widget->Handle->GetAllocation();
    return { rect.left, rect.top, rect.width, rect.height };
}

const char* sfgWidget_GetName(sfgWidget* Widget)
{
    Widget->String = Widget->Handle->GetName();
    return Widget->String.c_str();
}

const char* sfgWidget_GetClass(sfgWidget* Widget)
{
    Widget->String = Widget->Handle->GetClass();
    return Widget->String.c_str();
}

const char* sfgWidget_GetId(sfgWidget* Widget)
{
    Widget->String = Widget->Handle->GetId();
    return Widget->String.c_str();
}

sfInt32 sfgWidget_GetHierarchyLevel(sfgWidget* Widget)
{
    return Widget->Handle->GetHierarchyLevel();
}

sfgContainer* sfgWidget_GetParent(sfgWidget* Widget)
{
    sfgContainer* container = new sfgContainer;
    container->Handle = Widget->Handle->GetParent();
    return container;
}

sfVector2f sfgWidget_GetRequisition(sfgWidget* Widget)
{
    sf::Vector2f size = Widget->Handle->GetAbsolutePosition();
    return { size.x, size.y };
}

sfgWidgetState sfgWidget_GetState(sfgWidget* Widget)
{
    return static_cast<sfgWidgetState>(Widget->Handle->GetState());
}

sfgRendererViewport* sfgWidget_GetViewport(sfgWidget* Widget)
{
    sfgRendererViewport* renderer = new sfgRendererViewport;
    renderer->Handle = Widget->Handle->GetViewport();
    return renderer;
}

sfInt32 sfgWidget_GetZOrder(sfgWidget* Widget)
{
    return Widget->Handle->GetZOrder();
}

void sfgWidget_HandleAbsolutePositionChange(sfgWidget* Widget)
{
    Widget->Handle->HandleAbsolutePositionChange();
}

void sfgWidget_HandleEvent(sfgWidget* Widget, sfEvent* Event)
{
    sf::Event evt;
    ConvertBackEvent(*Event, &evt);
    Widget->Handle->HandleEvent(evt);
}

void sfgWidget_HandleGlobalVisibilityChange(sfgWidget* Widget)
{
    Widget->Handle->HandleGlobalVisibilityChange();
}

sfBool sfgWidget_HasFocus(sfgWidget* Widget)
{
    return Widget->Handle->HasFocus();
}

void sfgWidget_Invalidate(sfgWidget* Widget)
{
    Widget->Handle->Invalidate();
}

sfBool sfgWidget_IsActiveWidget(sfgWidget* Widget)
{
    return Widget->Handle->IsActiveWidget();
}

sfBool sfgWidget_IsGloballyVisible(sfgWidget* Widget)
{
    return Widget->Handle->IsGloballyVisible();
}

sfBool sfgWidget_IsLocallyVisible(sfgWidget* Widget)
{
    return Widget->Handle->IsLocallyVisible();
}

void sfgWidget_Refresh(sfgWidget* Widget)
{
    Widget->Handle->Refresh();
}

void sfgWidget_RequestResize(sfgWidget* Widget)
{
    Widget->Handle->RequestResize();
}

void sfgWidget_SetActiveWidget(sfgWidget* Widget)
{
    Widget->Handle->SetActiveWidget();
}

void sfgWidget_SetAllocation(sfgWidget* Widget, sfFloatRect Rect)
{
    Widget->Handle->SetAllocation({ Rect.left, Rect.top, Rect.width, Rect.height });
}

void sfgWidget_SetClass(sfgWidget* Widget, const char* Class)
{
    Widget->Handle->SetClass(Class);
}

void sfgWidget_SetHierarchyLevel(sfgWidget* Widget, sfInt32 Level)
{
    Widget->Handle->SetHierarchyLevel(Level);
}

void sfgWidget_SetId(sfgWidget* Widget, const char* Id)
{
    Widget->Handle->SetId(Id);
}

void sfgWidget_SetParent(sfgWidget* Widget, sfgWidget* Parent)
{
    Widget->Handle->SetParent(Parent->Handle);
}

void sfgWidget_SetPosition(sfgWidget* Widget, sfVector2f Position)
{
    Widget->Handle->SetPosition({ Position.x, Position.y });
}

void sfgWidget_SetRequisition(sfgWidget* Widget, sfVector2f Requisition)
{
    Widget->Handle->SetRequisition({ Requisition.x, Requisition.y });
}

void sfgWidget_SetState(sfgWidget* Widget, sfgWidgetState State)
{
    Widget->Handle->SetState(static_cast<sfg::Widget::State>(State));
}

void sfgWidget_SetViewport(sfgWidget* Widget, sfgRendererViewport* Viewport)
{
    Widget->Handle->SetViewport(Viewport->Handle);
}

void sfgWidget_SetZOrder(sfgWidget* Widget, sfInt32 ZOrder)
{
    Widget->Handle->SetZOrder(ZOrder);
}

void sfgWidget_Show(sfgWidget* Widget, sfBool Show)
{
    Widget->Handle->Show(static_cast<bool>(Show));
}

void sfgWidget_Update(sfgWidget* Widget, float Seconds)
{
    Widget->Handle->Update(Seconds);
}

void sfgWidget_UpdateDrawablePosition(sfgWidget* Widget)
{
    Widget->Handle->UpdateDrawablePosition();
}

void sfgWidget_Destroy(sfgWidget* Widget)
{
    delete Widget;
}

sfgObject* sfgWidget_GetBaseObject(sfgWidget* Widget)
{
    sfgObject* object = new sfgObject;
    object->Handle = Widget->Handle;
    return object;
}

sfBool sfgWidget_IsEqual(sfgWidget* A, sfgWidget* B)
{
    return A->Handle == B->Handle;
}
