#ifndef CSFGUI_IMAGESTRUCT_H
#define CSFGUI_IMAGESTRUCT_H

#include <SFGUI/Image.hpp>
#include <SFML/Graphics/Image.h>

struct sfgImage
{
    sfg::Image::Ptr Handle;
};

#endif
